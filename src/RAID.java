import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Kate on 2016-11-12.
 */

public class RAID implements RAIDInterface {
    private volatile DiskInterface[] diskInterfaces;
//    private volatile AtomicBoolean isShutDown = new AtomicBoolean(false);
    private volatile boolean isShutDown = false;
    private volatile int errorDiskNumber = -1;
    private volatile AtomicBoolean[] usedSectors;
    private int numberOfDisks;
    private int diskSize;
    private int sumDiskNumber;
    private int checkSum;

    public RAID() {
    }

    @Override
    public void addDisks(DiskInterface[] array) {
        this.diskInterfaces = array;
        numberOfDisks = array.length;
        diskSize = array[0].size();//every disk is equal
        sumDiskNumber = array.length - 1;//last disk is for recovery
        checkSum = (array.length - 1) * array[0].size();//every disk is equal
    }

    @Override
    public void write(int sector, int value) {
        int diskNumber = sector / diskSize;
        int physicalSectorNumber = sector % diskSize;
        synchronized (diskInterfaces[diskNumber]) {
            boolean writeError = false;
            int oldValueForCheckSum = 0;
            if (errorDiskNumber != diskNumber && errorDiskNumber != sumDiskNumber /*&& !isShutDown*//*.get()*/) {//zapis + save data
                try {
                    if (!isShutDown)
                        oldValueForCheckSum = diskInterfaces[diskNumber].read(physicalSectorNumber);
                    if (!isShutDown) {
                        diskInterfaces[diskNumber].write(physicalSectorNumber, value);
                    }
                } catch (DiskInterface.DiskError diskError) {
                    errorDiskNumber = diskNumber;
                    writeError = true;
                }
            }
                synchronized (diskInterfaces[sumDiskNumber]) {
                    if (!writeError /*&& !isShutDown*//*.get()*/) {
                        try {
                            int newCheckSumValue = 0;
                            if(!isShutDown) {
                                newCheckSumValue = value - oldValueForCheckSum + diskInterfaces[sumDiskNumber].read(physicalSectorNumber);//nowaSuma = staraSuma + nowaWartosc - starawartosc
                            }
                            if(!isShutDown) {
                                diskInterfaces[sumDiskNumber].write(physicalSectorNumber, newCheckSumValue);
                            }
                        } catch (DiskInterface.DiskError diskError) {
                            errorDiskNumber = sumDiskNumber;
                        }
                    } else if (writeError) {
                        //Zapis na dysk awaryjny Bo dysk się zepsuł !!!!!!!!!!!!!!!!
                    }
                }
            //}
            /*else*/ if(errorDiskNumber != diskNumber && errorDiskNumber == sumDiskNumber && !isShutDown/*.get()*/) {//tylko zapis, zepsuty dysk rezerwowy
                try {
                    diskInterfaces[diskNumber].write(physicalSectorNumber, value);
                } catch (DiskInterface.DiskError diskError) {
                    errorDiskNumber = sumDiskNumber;
                }
            }
        }
    }

    @Override
    public int read(int sector) {
        int diskNumber = sector / diskSize;
        int physicalSectorNumber = sector % diskSize;
        synchronized (diskInterfaces[diskNumber]) {
            if (errorDiskNumber != diskNumber && !isShutDown/*.get()*/) {//read data
                try {
                    return diskInterfaces[diskNumber].read(physicalSectorNumber);
                } catch (DiskInterface.DiskError diskError) {
                    errorDiskNumber = diskNumber;
                    return restoreDate(physicalSectorNumber);
                }
            }

            else if (errorDiskNumber == diskNumber && !isShutDown/*.get()*/) {//restore data
                return restoreDate(physicalSectorNumber);
            }

            else
                return -1;
        }
    }

    @Override
    public int size() {
        return checkSum;
    }

    @Override
    public void shutdown() {
//        isShutDown.set(true);
        isShutDown = true;
    }

    private int restoreDate(int sector){
//        return -1;
        int dataRecovered = 0;
        synchronized (diskInterfaces[sumDiskNumber]) {
            try {
                dataRecovered = diskInterfaces[sumDiskNumber].read(sector);
            } catch (DiskInterface.DiskError diskError) {
                errorDiskNumber = sumDiskNumber;
            }
        }
        for(int i = 0; i < numberOfDisks - 1; i++){
            if(i != errorDiskNumber) {
                synchronized (diskInterfaces[i]) {
                    try {
                        dataRecovered -= diskInterfaces[i].read(sector);
                    } catch (DiskInterface.DiskError diskError) {
                        errorDiskNumber = i;
                    }
                }
            }
        }
        return dataRecovered;
    }
}
