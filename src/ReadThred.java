/**
 * Created by epakarb on 2016-11-14.
 */
public class ReadThred implements Runnable {
    RAID raid;
    int id;
    int[] sectors;

    public ReadThred(RAID raid, int id, int[] sectors) {
        this.raid = raid;
        this.id = id;
        this.sectors = sectors;
    }

    @Override
    public void run() {
        for(int i = 0; i < sectors.length; i++) {
            System.out.println("tu wątek read: " + id + " odczytuje w: " + sectors[i]);
            System.out.println(raid.read(sectors[i]));
        }
    }
}
