/**
 * Created by epakarb on 2016-11-14.
 */
public class WriteThred implements Runnable {
    RAID raid;
    int id;
    int[] sectors;
    int[] values;

    public WriteThred(RAID raid, int id, int[] sectors, int[] values) {
        this.raid = raid;
        this.id = id;
        this.sectors = sectors;
        this.values = values;
    }

    @Override
    public void run() {
        for(int i = 0; i < sectors.length; i++) {
            System.out.println("tu wątek write: " + id + " zapisuje w: " + sectors[i] + " wartość: " + values[i]);
            raid.write(sectors[i], values[i]);
        }
    }
}
