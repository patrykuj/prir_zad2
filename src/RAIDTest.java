/**
 * Created by Kate on 2016-11-13.
 */
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;


public class RAIDTest {
    private Disk[] diskInterfaces;
    private RAID raid;

    @Before
    public void setUp(){
        diskInterfaces = new Disk[5];
        for(int i = 0; i < 5; i++)
            diskInterfaces[i] = new Disk(50);

        raid = new RAID();
        raid.addDisks(diskInterfaces);
    }

    @Test
    public void readWriteCountTest() throws InterruptedException{
        assertEquals("Check size of logical disk ( (all disk - 1) * size of disk )", 200, raid.size());
        WriteThred[] writeThred = new WriteThred[5];
        ReadThred[] readThreds = new ReadThred[5];

        Thread[] tWrite = new Thread[5];
        Thread[] tRead = new Thread[5];

        int[] sectorstoWrite = {0, 10, 70, 72, 180};
        int[][] valuesToWrite = {{-1, -50, -51, -100, -199}, {2, 52, 53, 101, 299},
                                {2, 20, 21, 200, 399}, {4, 40, 41, 400, 499},
                                {5, 50, 51, 500, 599}};

        for (int i = 0; i < 5; i++) {
            writeThred[i] = new WriteThred(raid, i, sectorstoWrite, valuesToWrite[i]);
            readThreds[i] = new ReadThred(raid, i, sectorstoWrite);
            tWrite[i] = new Thread(writeThred[i]);
            tRead[i] = new Thread(readThreds[i]);
        }

        for (int i = 0; i < 5; i++) {
            System.out.println("i: " + i);
            tWrite[i].start();
        }

        for (int i = 0; i < 5; i++){
            try {
                tWrite[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Thread.sleep(1000);

        for (int i = 0; i < 5; i++) {
            System.out.println("i: " + i);
            tRead[i].start();
        }

        for (int i = 0; i < 5; i++){
            try {
                tRead[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Thread.sleep(1000);

        raid.shutdown();


        System.out.println("read test after shoutdown" + raid.read(10));
        raid.write(10, 9999);


        for(Disk di: diskInterfaces){
            System.out.println(Arrays.toString(di.disk));
            System.out.println("write: " + di.writeCounter);
            System.out.println("readcounter: " + di.readCounter);
        }

//        try {
//            Thread.sleep(10000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

    }

}
