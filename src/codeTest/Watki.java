package codeTest;

/**
 * Created by Kate on 2016-11-12.
 */
public class Watki implements Runnable{
    static Zasob zasob = new Zasob();
    int id;

    public Watki(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        System.out.println("Tu wątek " + id + " spróbuje dostać zasób 1");
//        synchronized (Zasob.zasoby[0]){
//            System.out.println("Tu wątek " + id + " mam zasób 1: " + Zasob.zasoby[0]);
//            try {
//                Thread.sleep(10000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
        zasob.getZasob(id, id);
        System.out.println("Kończe pracę, wątek: " + id);
    }
}
