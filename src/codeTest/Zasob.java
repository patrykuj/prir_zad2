package codeTest;

/**
 * Created by Kate on 2016-11-12.
 */
public class Zasob {
    public static String[] zasoby = {"zasob1", "zasob2", "zasob3", "zasob4", "zasob5"};

    public String getZasob(int id, int nr){
        synchronized (zasoby[nr]){
            System.out.println("Daje zasob dla watka nr: " + id + " -dlugo");
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return zasoby[0];
        }
    }
}
